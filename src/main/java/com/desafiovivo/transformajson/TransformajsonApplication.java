package com.desafiovivo.transformajson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransformajsonApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransformajsonApplication.class, args);
	}

}
